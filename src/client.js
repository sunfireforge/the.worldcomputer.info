const blessed = require('blessed');
const net = require('net');

// Create a Blessed screen
const screen = blessed.screen({
  smartCSR: true,
});

// Create a text input box
const inputBox = blessed.textbox({
  top: 'center',
  left: 'center',
  width: '50%',
  height: 'shrink',
  content: 'Enter text and press Enter to send:',
  border: 'line',
  keys: true,
  vi: true,
  mouse: true,
  inputOnFocus: true
});

// Append the input box to the screen
screen.append(inputBox);

// Focus the input box
inputBox.focus();

// Create a Telnet client
const client = new net.Socket();

// Connect to the server
client.connect(3000, 'localhost', () => {
  console.log('Connected to server');

  // Handle input events on the input box
  inputBox.on('submit', (value) => {
    // Send the entered text to the server
    client.write(value);
    inputBox.clearValue();
    screen.render();
  });
});

// Handle data received from the server
client.on('data', (data) => {
  // Display the received data in the console
  console.log('Received:', data.toString());
});

// Handle errors
client.on('error', (err) => {
  console.error('Error:', err.message);
});

// Handle server connection closed
client.on('close', () => {
  console.log('Connection closed');
});

// Render the screen
screen.render();
