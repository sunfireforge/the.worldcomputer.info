const blessed = require('blessed');

// Create a screen
const screen = blessed.screen({
  smartCSR: true,
});

// Create a grid layout
const grid = new blessed.grid({
  rows: 3,
  cols: 3,
  screen: screen
});

// Create and add elements to the grid
const box1 = blessed.box({
  content: 'Box 1',
  style: {
    fg: 'white',
    bg: 'blue'
  }
});
grid.set(0, 0, 1, 1, box1);

const box2 = blessed.box({
  content: 'Box 2',
  style: {
    fg: 'white',
    bg: 'green'
  }
});
grid.set(0, 1, 1, 1, box2);

const box3 = blessed.box({
  content: 'Box 3',
  style: {
    fg: 'white',
    bg: 'red'
  }
});
grid.set(1, 0, 2, 2, box3);

// Append the grid to the screen
screen.append(grid);

// Quit on Escape, q, or Control-C
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Render the screen
screen.render();