const fs = require('fs');
const blessed = require('blessed');

function ROOT(path) {
    return "../" + path
}

// Read the content of a file
const fileContent = fs.readFileSync(ROOT("public/ansi/welcome.ans"), 'utf8');

// Create a screen object
const screen = blessed.screen({
  smartCSR: true,
  title: 'File Content Example'
});


layout = "" //layout function

// Append the box to the screen
screen.append(layout);

// Quit on Escape, q, or Control-C
screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Focus the box
box.focus();

// Render the screen
screen.render();