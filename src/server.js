const net = require('net');
const blessed = require('blessed');

// Create a Blessed screen for each Telnet client
function createScreen(socket) {
    const screen = blessed.screen({
        input: socket,
        output: socket
    });

    // Create and append Blessed elements to the screen
    const box = blessed.box({
        width: '80%',
        height: '80%',
        content: 'Welcome to My Blessed Application!',
        tags: true,
        border: {
            type: 'line'
        },
        style: {
            fg: 'white',
            border: {
                fg: '#f0f0f0'
            },
            hover: {
                bg: 'green'
            }
        }
    });
    screen.append(box);

    // Handle key presses
    screen.key(['q', 'C-c'], function(ch, key) {
        screen.destroy();
    });

    // Handle screen resize
    screen.on('resize', function() {
        // Update layout on resize
        screen.render();
    });

    return screen;
}

// Create a Telnet server
const server = net.createServer((socket) => {
    console.log('Client connected');

    // Create a Blessed screen for the client
    const screen = createScreen(socket);

    // Handle client disconnect
    socket.on('end', () => {
        console.log('Client disconnected');
        screen.destroy();
    });

    // Handle errors
    socket.on('error', (err) => {
        console.error('Client error:', err.message);
        screen.destroy();
    });
});

// Start listening on a specific port
const PORT = 3000;
server.listen(PORT, () => {
    console.log(`Telnet server listening on port ${PORT}`);
});
